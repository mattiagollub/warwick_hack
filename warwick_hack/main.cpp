

#include <iostream>
#include <chrono>
#include <thread>

#ifdef _WIN32
#include <Windows.h>
#endif

#include <myo/myo.hpp>

#include "tile_screen.hpp"
#include "data_collector.hpp"

#include <GLFW\glfw3.h>
#include <gl\GL.h>
#include <gl\GLU.h>

void error_callback(int error, const char* description) {
	std::cout << "[GLFW ERROR]: %s", description;
}

float x_position = 0.0;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
		x_position += 10.0;
	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
		x_position -= 10.0;
}

int main(int argc, char *arg) {
	
	// Test code for syn_engine
	std::cout << "Starting demo...";
	int width = 1280;
	int height = 720;
	glutInit(&argc, &arg);

	// Get Myo device
	myo::Hub hub("com.example.hello-myo");
	std::cout << "Attempting to find a Myo..." << std::endl;
	myo::Myo* myo = hub.waitForMyo(10000);

	// If waitForAnyMyo() returned a null pointer, we failed to find a Myo, so exit with an error message.
	if (!myo) {
		throw std::runtime_error("Unable to find a Myo!");
	}

	// We've found a Myo.
	std::cout << "Connected to a Myo armband!" << std::endl << std::endl;

	// Next we construct an instance of our DeviceListener, so that we can register it with the Hub.
	DataCollector collector;
	hub.addListener(&collector);
	hub.run(1000 / 5);
	myo_status relax = collector.get_status();

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		std::cout << "Failed to initialize GLFW.";

	// GLFWwindow* window = glfwCreateWindow(width, height, "Financial data viewer", glfwGetPrimaryMonitor(), NULL);
	GLFWwindow* window = glfwCreateWindow(width, height, "Financial data viewer", NULL, NULL);
	
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	// Generate background texture
	GLuint texture;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	uchar* pixels = (uchar*)malloc(width * height * 4);
	for (size_t i = 0; i < width * height * 4; i++) pixels[i] = 0;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	free(pixels);

	size_t tile_width = 350;
	size_t tile_height = 180;
	size_t y_min = 50;
	size_t x_margin = 20;
	size_t y_margin = 20;
	tile_screen screen(tile_width, tile_height);

	std::chrono::time_point<std::chrono::system_clock> prev, now;
	prev = std::chrono::system_clock::now();

	bool move = false;
	bool gesture_active = false;

	while (!glfwWindowShouldClose(window)) {

		float ratio = (float)width / (float)height;
		now = std::chrono::system_clock::now();
		float elapsed_seconds = (float)std::chrono::duration_cast<std::chrono::nanoseconds>(now - prev).count() / 1000000.0;
		prev = now;

		// In each iteration of our main loop, we run the Myo event loop for a set number of milliseconds.
		// In this case, we wish to update our display 20 times a second, so we run for 1000/20 milliseconds.
		hub.run(1000 / 50);
		myo_status status = collector.get_status();
		
		if (status.pose == myo::Pose::fingersSpread && !gesture_active) {
			move = !move;
			gesture_active = true;

			if (move) {
				relax = collector.get_status();
			}
		}
		else if (status.pose != myo::Pose::fingersSpread) {
			gesture_active = false;
		}

		
		if (move)
			x_position += 0.3 * elapsed_seconds * (status.yaw - relax.yaw);

		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, width, 0.0, height, -1.0, 1);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glClearColor(.3f, .3f, .3f, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Draw background
		// glEnable(GL_TEXTURE_2D);
		// glBindTexture(GL_TEXTURE_2D, texture);
		// glBegin(GL_QUADS);
		// glTexCoord2f(0, 0); glVertex2f(0, 0);
		// glTexCoord2f(0, 1); glVertex2f(0, height);
		// glTexCoord2f(1, 1); glVertex2f(width, height);
		// glTexCoord2f(1, 0); glVertex2f(width, 0);
		// glEnd();

		// Draw tiles
		float x = x_margin + x_position;
		float y = y_margin + y_min;

		screen.access_lock.lock();
		for (size_t i = 0; i < screen.tiles.size(); i++) {
			tile& t = screen.tiles[i];
			if ((float)t.height + y > (float)height) {
				y = y_margin + y_min;
				x += (float)t.width + x_margin;
			}

			if (true) {
				t.draw(x, y);
				y += y_margin + (float)t.height;
			}
		}
		screen.access_lock.unlock();

		glFlush();
		glFinish();


		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
}
	