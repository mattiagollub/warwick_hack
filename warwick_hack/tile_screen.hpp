
#include <vector>
#include <thread>
#include <fstream>
#include <sstream>
#include <string>
#include <mutex>

#include "tile.hpp"
#include "bloomberg.hpp"

using namespace std;

class tile_screen {

public:
	tile_screen(size_t width, size_t height) {

		std::thread updater([this, width, height] () {

			std::ifstream infile("top.txt");
			std::string line;
			while (std::getline(infile, line))
			{
				uchar* tilePixels = (uchar*)malloc(width * height * 4);
				for (size_t p = 0; p < width * height * 4; p += 4) {
					tilePixels[p] = 254;
					tilePixels[p + 1] = 128;
					tilePixels[p + 2] = 10;
					tilePixels[p + 3] = 10;
				}

				tile t = tile(width, height, line);
				t.update_tile(tilePixels);

				access_lock.lock();
				tiles.push_back(t);
				access_lock.unlock();

				free(tilePixels);
			}

			SimpleSubscriptionExample example;
			
			try {
				example.run(tiles);
			}
			catch (Exception &e) {
				std::cerr << "Unhandled error in the bloomberg library: " << e.description()
					<< std::endl;
			}
		});

		updater.detach();
	}


	std::mutex access_lock;
	vector<tile> tiles;
};