
#include <iostream>

#ifdef _WIN32
#include <Windows.h>
#endif

#include <GLFW\glfw3.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <gl\freeglut.h>
#include <gl\freeglut_ext.h>
#include <gl\freeglut_std.h>

typedef unsigned char uchar;

struct stock_data {
	std::string name;
	float value;
	float variation;
	// ...
};

class tile {

public:
	tile(size_t width, size_t height, std::string name)
		: width(width), height(height) {

		data.name = name;
		data.value = 0.0;
		data.variation = 0.0;

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}

	void update_tile(uchar* pixels) {
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	}

	void draw(float offsetX, float offsetY) {

		glColor3f(1.0f, 1.0f, 1.0f);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex2f(offsetX, offsetY);
		glTexCoord2f(0, 1); glVertex2f(offsetX, (float)height + offsetY);
		glTexCoord2f(1, 1); glVertex2f((float)width + offsetX, (float)height + offsetY);
		glTexCoord2f(1, 0); glVertex2f((float)width + offsetX, offsetY);
		glEnd();

		std::string::size_type idx;

		glColor3f(0.0f, 0.0f, 0.0f);
		glRasterPos2f(offsetX + 20, offsetY + height - 40);
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*)data.name.c_str());
		glRasterPos2f(offsetX + 20, offsetY + height - 100);
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*)std::to_string(data.value).c_str());
		glRasterPos2f(offsetX + 20, offsetY + height - 160);
		glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*)std::to_string(data.variation).c_str());

		glColor3f(1.0f, 1.0f, 1.0f);
	}

	void update(float new_price) {
		data.variation = (new_price - data.value) / data.value;
		if (data.variation > 500.0) data.variation = 0.0;
		data.value = new_price;

		uchar* tilePixels = (uchar*)malloc(width * height * 4);
		
		if (data.value > 800.0) {
			for (size_t p = 0; p < width * height * 4; p += 4) {
				tilePixels[p] = 10;
				tilePixels[p + 1] = 200;
				tilePixels[p + 2] = 10;
				tilePixels[p + 3] = 255;
			}
		}
		else {
			for (size_t p = 0; p < width * height * 4; p += 4) {
				tilePixels[p] = 150;
				tilePixels[p + 1] = 150;
				tilePixels[p + 2] = 10;
				tilePixels[p + 3] = 255;
			}
		}

		update_tile(tilePixels);
		free(tilePixels);
	}

	GLuint texture;
	size_t height;
	size_t width;
	stock_data data;
};