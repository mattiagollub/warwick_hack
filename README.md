# README #

## Myo-controlled stock prices viewer ##

We wanted to harness the potential of the hardware available to us over the 24 hours to create a dynamic method of viewing the Bloomberg Platform. We created a motion orientated code synchronised to bloomberg's financial data in order to view share prices, price changes and graphs of a variety of stocks.